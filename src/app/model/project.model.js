"use strict";
var Project = (function () {
    function Project(id, name) {
        this.id = id;
        this.name = name;
    }
    return Project;
}());
exports.Project = Project;
//# sourceMappingURL=project.model.js.map
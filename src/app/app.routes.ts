import {Routes} from "@angular/router";
import {HomeComponent} from "./component/home/home.component";
import {ProjectsComponent} from "./component/projects/projects.component";
import {ProjectDetailComponent} from "./component/projects/detail/project.component";
import {SkillsComponent} from "./component/skills/skills.component";
import {NotFoundComponent} from "./shared/not-found/not-found.component";
import {WorkshopComponent} from "./component/workshop/workshop.component";

export const appRoutes: Routes = [
    {
        path: "",
        pathMatch: "full",
        component: HomeComponent
    },
    {
        path: "projects",
        component: ProjectsComponent,
    },
    {
        path: "projects/:id",
        component: ProjectDetailComponent
    },
    {
        path: "skills",
        component: SkillsComponent
    },
    {
        path: "workshop",
        component: WorkshopComponent
    },
    {
        path: "not-found",
        component: NotFoundComponent
    },
    {
        path: "**",
        component: NotFoundComponent
    }
];


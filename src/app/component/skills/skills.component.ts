import {Component} from "@angular/core";
import {SkillService} from "../../service/skill.service";

@Component({
    selector: 'skills',
    templateUrl: "skills.component.html",
    styleUrls: ["skills.component.css"]
})
export class SkillsComponent {
    skills: string[];

    constructor(skillService: SkillService) {
        this.skills = skillService.load();
    }
}
import {Component, OnInit} from "@angular/core";
import {ProjectService} from "../../service/project.service";
import {Project} from "../../model/project.model";

@Component({
    selector: 'projects',
    templateUrl: "projects.component.html",
    styleUrls: ["projects.component.css"]
})
export class ProjectsComponent implements OnInit {
    projects: Project[] = [];

    constructor(private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.projects = this.projectService.load();
    }

}
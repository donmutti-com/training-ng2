import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../../../service/project.service";
import {Project} from "../../../model/project.model";

@Component({
    selector: 'project-detail',
    templateUrl: "project.component.html",
    styleUrls: ["project.component.css"]
})
export class ProjectDetailComponent implements OnInit {

    private project: Project;

    constructor(public route: ActivatedRoute, public router: Router, private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
                let projectId = params['id'];
                this.project = this.projectService.loadOne(projectId);
                if (!this.project) {
                    this.router.navigate(["not-found"], {skipLocationChange: true});
                }
            }
        );
    }
}
"use strict";
var project_model_1 = require("../model/project.model");
var ProjectService = (function () {
    function ProjectService() {
        this.allData = [
            new project_model_1.Project(1, "Project-1"),
            new project_model_1.Project(2, "Project-2"),
            new project_model_1.Project(3, "Project-3")
        ];
    }
    ProjectService.prototype.load = function () {
        return this.allData;
    };
    ProjectService.prototype.loadOne = function (id) {
        if (id >= 1 && id <= 3) {
            return this.allData[id - 1];
        }
        return null;
    };
    return ProjectService;
}());
exports.ProjectService = ProjectService;
//# sourceMappingURL=project.service.js.map
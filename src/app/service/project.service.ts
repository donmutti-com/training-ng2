import {Project} from "../model/project.model";

export class ProjectService {

    private allData: Project[] = [
        new Project(1, "Project-1"),
        new Project(2, "Project-2"),
        new Project(3, "Project-3")
    ];

    load(): Project[] {
        return this.allData;
    }

    loadOne(id: number) {
        if (id >= 1 && id <= 3) {
            return this.allData[id-1];
        }
        return null;
    }
}
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";
import {AutoGrowDirective} from "./directive/auto-grow.directive";
import {AppHeaderComponent} from "./shared/app-header/app-header.component";
import {AppFooterComponent} from "./shared/app-footer/app-footer.component";
import {NotFoundComponent} from "./shared/not-found/not-found.component";
import {AppComponent} from "./app.component";
import {appRoutes} from "./app.routes";
import {HomeComponent} from "./component/home/home.component";
import {ProjectService} from "./service/project.service";
import {ProjectsComponent} from "./component/projects/projects.component";
import {ProjectDetailComponent} from "./component/projects/detail/project.component";
import {SkillService} from "./service/skill.service";
import {SkillsComponent} from "./component/skills/skills.component";
import {WorkshopComponent} from "./component/workshop/workshop.component";

@NgModule({
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
        HttpModule
    ],
    exports: [RouterModule],
    declarations: [
        AutoGrowDirective,
        AppComponent,
        AppHeaderComponent,
        AppFooterComponent,
        NotFoundComponent,
        HomeComponent,
        ProjectsComponent,
        ProjectDetailComponent,
        SkillsComponent,
        WorkshopComponent
    ],
    providers: [
        {provide: "Window", useValue: window},
        ProjectService,
        SkillService
    ]
})
export class AppModule { }

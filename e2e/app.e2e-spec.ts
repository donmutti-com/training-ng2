import { TrainingNg2CliPage } from './app.po';

describe('training-ng2-cli App', function() {
  let page: TrainingNg2CliPage;

  beforeEach(() => {
    page = new TrainingNg2CliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
